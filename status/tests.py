from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from status import views, models, forms
from .views import status_page
from .models import Status
from .forms import Status_Form
from project6.settings import BASE_DIR
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from datetime import datetime
import time
import os

c = Client()

# Create your tests here.
class UnitTest(TestCase):
    def test_apakah_ada_url_benar(self):
        response = c.get('')
        self.assertEqual(response.status_code, 200)

    def test_apakah_tidak_ada_url_slash_status(self):
        response = c.get('/status/')
        self.assertEqual(response.status_code, 404)

    def test_apakah_pake_fungsi_status_page(self):
        response = resolve('/')
        self.assertEqual(response.func, views.status_page)

    def test_apakah_pake_template_status_html(self):
        response = c.get('')
        self.assertTemplateUsed(response, 'status.html')

    def test_apakah_ada_tulisan_halo_apa_kabar(self):
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn("Halo, apa kabar?", content)

    def test_apakah_ada_button_dengan_tulisan_submit(self):
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn('<input id="status-tombol" type="submit" value="Add Status" name="tombol">', content)

    def test_apakah_model_bisa_buat_status_baru_atau_engga(self):
        new_status = Status.objects.create(status='frustasi')
        self.assertEqual(Status.objects.all().count(), 1)
        
class FuncTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(FuncTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FuncTest, self).tearDown()

    def test_apakah_formnya_bisa_apa_engga(self):
        self.selenium.get(self.live_server_url)

        temp_input = self.selenium.find_element_by_id("status-input")
        temp_input.send_keys('Mantap')

        time.sleep(3)

        temp_button = self.selenium.find_element_by_id("status-tombol")
        temp_button.submit()

        time.sleep(3)

        self.assertIn('Mantap', self.selenium.page_source)