# Story-6 PPW

Repository ini merupakan hasil pengerjaan story-6 untuk mata kuliah Perancangan & Pemrograman Web.

## Heroku

Klik [disini](https://ctupa7-story6.herokuapp.com/) untuk melihat website.

## Pipeline and Coverage

[![pipeline status](https://gitlab.com/ctupa7/story6/badges/master/pipeline.svg)](https://gitlab.com/ctupa7/story6/commits/master)
[![coverage report](https://gitlab.com/ctupa7/story6/badges/master/coverage.svg)](https://gitlab.com/ctupa7/story6/commits/master)