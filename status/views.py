from django.shortcuts import render
from .forms import Status_Form
from .models import Status
from datetime import datetime

# Create your views here.
def status_page(request):
    if request.method == "POST":
        form = Status_Form(request.POST)
        if form.is_valid():
            form.save()
    
    stats = Status.objects.all().order_by('-date_created')
    form = Status_Form()
    arguments = {
        'status': stats,
        'form': form,
    }
    return render(request, 'status.html', arguments)
